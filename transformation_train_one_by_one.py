from keras.layers import Input, Dense, Conv2D, MaxPooling2D, UpSampling2D,Lambda,Multiply,Reshape,Dropout,concatenate,Flatten,LocallyConnected2D,Conv2DTranspose,BatchNormalization,Concatenate
from keras.models import Model
from keras.optimizers import Adam,SGD
import cv2
import os
import numpy
import random
import mnist


NDX=3
NDY=3
NA=5
NSX=3
NSY=3
N=3*3*5*3*3





#from transformation index to
def getParams(index):
    dxi=index%NDX
    dyi=int(index/NDX)%NDY
    dai=int(index/(NDX*NDY))%NA
    dsxi=int(index/(NDX*NDY*NA))%NSX
    dsyi=int(index/(NDX*NDY*NA*NSX))

    return (dxi-int(NDX/2))*3.0,(dyi-int(NDY/2))*3.0,(dai-int(NA/2))*15.0,(dsxi-int(NSX/2))*0.15+1.0,(dsyi-int(NSY/2))*0.15+1.0

#transform image
def transform(img,dx,dy,alpha,sx,sy):

    M = numpy.float32([[sx,0,dx],[0,sy,dy]])
    dst = cv2.warpAffine(img,M,(img.shape[1],img.shape[0]))
    M = cv2.getRotationMatrix2D((img.shape[1]/2, img.shape[0]/2), alpha, 1.0)
    res=cv2.warpAffine(dst,M,(img.shape[1],img.shape[0]))
    return res

#Keras model


#Keras model
def createAeModel():
    input = Input(shape=(28,28))

    x=Reshape((28*28,))(input)
    x=Dense(1024,activation='relu')(x)
    x=Dropout(0.1)(x)
    x=Dense(512,activation='relu')(x)
    x=Dropout(0.1)(x)

    code=Dense(32,activation='linear')(x)

    x=Dense(512,activation='relu',name='decode1')(code)
    x=Dropout(0.1)(x)
    x=Dense(1024,activation='relu',name='decode2')(x)
    x=Dropout(0.1)(x)
    x=Dense(784,activation='sigmoid',name='decode3')(x)
    output=Reshape((28,28))(x)

    return Model(inputs=input,outputs=[output,code])
def createDecoderModel():
    code=Input(shape=(32,))

    x=Dense(512,activation='relu',name='decode1')(code)
    x=Dropout(0.1)(x)
    x=Dense(1024,activation='relu',name='decode2')(x)
    x=Dropout(0.1)(x)
    x=Dense(784,activation='sigmoid',name='decode3')(x)
    output=Reshape((28,28))(x)

    return Model(inputs=code,outputs=output)

def createModel():
    input = Input(shape=(32,))

    hidden=Dense(512,activation='relu')(input)
    output=Dense(32,activation='linear')(hidden)
    return Model(inputs=input,outputs=output)

#prepair data from mnist to a numpy array
def getNumpyList(images):
    res=[]
    for i in range(len(images)):
        temp=numpy.array(images[i])
        img=numpy.reshape(temp,(28,28))
        img=img.astype(dtype=numpy.float)/255.0
        res.append(img)
    return res

#load mnist data
mndata=mnist.MNIST('dataset')
images_train, labels_train = mndata.load_training()
images_test, labels_test = mndata.load_testing()
train_data=getNumpyList(images_train)

#Autoencoder model
modelAe=createAeModel()
modelAe.load_weights('mnist_ae.h5')

#this is the way of creating decoder model and copying weights from the autoencoder
decoder=createDecoderModel()
decoder.get_layer('decode1').set_weights(modelAe.get_layer('decode1').get_weights())
decoder.get_layer('decode2').set_weights(modelAe.get_layer('decode2').get_weights())
decoder.get_layer('decode3').set_weights(modelAe.get_layer('decode3').get_weights())

codes=None
numpy_result=numpy.zeros((int(len(train_data)),N,32))

#prepair codes if 'codes.npy' doesn't exist and save codes.npy, or just load it
if not os.path.isfile('codes.npy'):
    for i in range(int(len(train_data))):
        numpy_train_data=numpy.zeros((N,28,28))
        for j in range(N):
            index=j
            dx,dy,a,sx,sy=getParams(index)
            trans=transform(train_data[i],dx,dy,a,sx,sy)

            if j==0:
                cv2.imwrite('0.bmp',trans*255.0)
            if j==N-1:
                cv2.imwrite(str(N-1)+'.bmp',trans*255.0)

            numpy_train_data[j,:,:]=trans[:,:]
        result=modelAe.predict(numpy_train_data)
        numpy_result[i,:,:]=result[1][:,:]
        if i%10==0:
            print(str(i))
    numpy.save('codes',numpy_result)
    codes=numpy_result
else:
    codes=numpy.load('codes.npy')

TRAIN_SIZE = 10000
numpy_train_data=numpy.zeros((TRAIN_SIZE,32))
numpy_train_data_trans=numpy.zeros((TRAIN_SIZE,32))

#train each transformation
for index in range(N):
    model=createModel()

    model.compile(optimizer=Adam(lr=0.01), loss="mean_squared_error")
    history=None
    for ep in range(100):


        for i in range(TRAIN_SIZE):
            j=random.randint(0,codes.shape[0]-1)
            index0=int(N/2)

            numpy_train_data[i,:]=codes[j,index0,:]
            numpy_train_data_trans[i,:]=codes[j,index,:]

        history=model.fit(numpy_train_data,numpy_train_data_trans,256,1)
        model.save_weights('trans_weights/trans'+str(index)+'.h5')

    with open("losses.txt", "a") as myfile:
        myfile.write(str(history.history['loss'][0])+'\n')




