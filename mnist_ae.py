from keras.layers import Input, Dense, Conv2D, MaxPooling2D, UpSampling2D,Lambda,Multiply,Reshape,Dropout,concatenate,Flatten,Conv2DTranspose,BatchNormalization,Concatenate
from keras.models import Model
from keras.optimizers import Adam,SGD
from keras import backend as K
import cv2
import os
import numpy
from PIL import Image
import random
import time
import math

import functools
from keras import backend as K
import tensorflow as tf

NDX=5
NDY=5
NA=7
NSX=3
NSY=3
N=5*5*7*3*3


import mnist

#from transformation index to
def getParams(index):
    dxi=index%5
    dyi=int(index/5)%5
    dai=int(index/25)%7
    dsxi=int(index/175)%3
    dsyi=int(index/525)

    return (dxi-NDX/2)*2.0,(dyi-NDY/2)*2.0,(dai-NA/2)*5.0,(dsxi-NSX/2)*0.15+1.0,(dsyi-NSY/2)*0.15+1.0

#transform image
def transform(img,dx,dy,alpha,sx,sy):

    M = numpy.float32([[sx,0,dx],[0,sy,dy]])
    dst = cv2.warpAffine(img,M,(img.shape[1],img.shape[0]))
    M = cv2.getRotationMatrix2D((img.shape[1]/2, img.shape[0]/2), alpha, 1.0)
    res=cv2.warpAffine(dst,M,(img.shape[1],img.shape[0]))
    return res

#Keras model
def createModel():
    input = Input(shape=(28,28))

    x=Reshape((28*28,))(input)
    x=Dense(1024,activation='relu')(x)
    x=Dropout(0.1)(x)
    x=Dense(512,activation='relu')(x)
    x=Dropout(0.1)(x)

    code=Dense(32,activation='linear')(x)

    x=Dense(512,activation='relu',name='decode1')(code)
    x=Dropout(0.1)(x)
    x=Dense(1024,activation='relu',name='decode2')(x)
    x=Dropout(0.1)(x)
    x=Dense(784,activation='sigmoid',name='decode3')(x)
    output=Reshape((28,28))(x)

    return Model(inputs=input,outputs=[output,code])

def getNumpyList(images):
    res=[]
    for i in range(len(images)):
        temp=numpy.array(images[i])
        img=numpy.reshape(temp,(28,28))
        img=img.astype(dtype=numpy.float)/255.0
        res.append(img)
    return res

mndata=mnist.MNIST('dataset')
images_train, labels_train = mndata.load_training()
images_test, labels_test = mndata.load_testing()

TRAIN_SIZE = 60000


model=createModel()
model.compile(optimizer=Adam(lr=0.001), loss=['mean_squared_error',None])#''binary_crossentropy')

model.load_weights('mnist_ae.h5')

train_data=getNumpyList(images_train)

'''img=train_data[0]
dx,dy,a,sx,sy=getParams(0)
res=transform(img,dx,dy,a,sx,sy)
cv2.imwrite('tr.bmp',res*255.0)'''


test_data=getNumpyList(images_test)
test_data=getNumpyList(images_test)

numpy_train_data=numpy.zeros((len(train_data),28,28))
numpy_test_data=numpy.zeros((len(test_data),28,28))
for i in range(len(test_data)):
    numpy_test_data[i,:,:]=test_data[i][:,:]



for ep in range(1000):
    result=model.predict(numpy_test_data)
    for i in range(100):
        cv2.imwrite('test/'+str(i)+'.bmp',result[0][i]*255.0)
        cv2.imwrite('test/'+str(i)+'_or.bmp',numpy_test_data[i]*255.0)


    #numpy_train_data_context=numpy.zeros((len(train_data),N,16))
    #numpy_train_data_transf=numpy.zeros((len(train_data),28,28))
    for i in range(len(train_data)):
        index=random.randint(0,N-1)
        dx,dy,a,sx,sy=getParams(index)
        trans=transform(train_data[i],dx,dy,a,sx,sy)

        numpy_train_data[i,:,:]=trans[:,:]

        #numpy_train_data[i,:,:]=train_data[i][:,:]

    model.fit(numpy_train_data,numpy_train_data,64,1,validation_data=[numpy_test_data,numpy_test_data])
    model.save_weights('mnist_ae.h5')


