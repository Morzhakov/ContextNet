from keras.layers import Input, Dense, Conv2D, LocallyConnected2D,MaxPooling2D, UpSampling2D,Lambda,Multiply,Reshape,Dropout,concatenate,Flatten,Conv2DTranspose,BatchNormalization,Concatenate
from keras.models import Model
import cv2
import os
import numpy
import mnist
import random

NDX=3
NDY=3
NA=5
NSX=3
NSY=3
N=3*3*5*3*3

#from transformation index to
def getParams(index):
    dxi=index%3
    dyi=int(index/3)%3
    dai=int(index/9)%5
    dsxi=int(index/45)%3
    dsyi=int(index/135)

    return (dxi-int(NDX/2))*2.0,(dyi-int(NDY/2))*2.0,(dai-int(NA/2))*10.0,(dsxi-int(NSX/2))*0.15+1.0,(dsyi-int(NSY/2))*0.15+1.0

#transform image
def transform(img,dx,dy,alpha,sx,sy):

    M = numpy.float32([[sx,0,dx],[0,sy,dy]])
    dst = cv2.warpAffine(img,M,(img.shape[1],img.shape[0]))
    M = cv2.getRotationMatrix2D((img.shape[1]/2, img.shape[0]/2), alpha, 1.0)
    res=cv2.warpAffine(dst,M,(img.shape[1],img.shape[0]))
    return res

#Keras model
def createAeModel(input):
    x=Reshape((28*28,))(input)
    x=Dense(1024,activation='relu')(x)
    x=Dropout(0.1)(x)
    x=Dense(512,activation='relu')(x)
    x=Dropout(0.1)(x)

    code=Dense(32,activation='linear')(x)

    x=Dense(512,activation='relu',name='decode1')(code)
    x=Dropout(0.1)(x)
    x=Dense(1024,activation='relu',name='decode2')(x)
    x=Dropout(0.1)(x)
    x=Dense(784,activation='sigmoid',name='decode3')(x)
    output=Reshape((28,28))(x)

    return Model(inputs=input,outputs=[output,code]),code

def createDecoderModel():
    code=Input(shape=(32,))
    x=Dense(512,activation='relu',name='decode1')(code)
    x=Dropout(0.1)(x)
    x=Dense(1024,activation='relu',name='decode2')(x)
    x=Dropout(0.1)(x)
    x=Dense(784,activation='sigmoid',name='decode3')(x)
    output=Reshape((28,28))(x)

    return Model(inputs=code,outputs=output)

def createTransformModel(code):
    hidden=Dense(N*512,activation='relu',name='hidden')(code)
    hidden_reshape=Reshape((N,1,512))(hidden)
    minicolumns_n=LocallyConnected2D(32,(1,1),activation='linear',name='output')(hidden_reshape)
    minicolumns=Reshape((N,32))(minicolumns_n)

    return minicolumns

def createModel():
    input = Input(shape=(28,28))
    modelAE,code=createAeModel(input)
    modelAE.load_weights('mnist_ae.h5')
    modelAE.trainable=False

    minicolumns=createTransformModel(code)

    return Model(inputs=input,outputs=[minicolumns,code]),modelAE

def createOneTransformModel():
    input = Input(shape=(32,))

    hidden=Dense(512,activation='relu',name='hidden')(input)
    output=Dense(32,activation='linear',name='output')(hidden)
    return Model(inputs=input,outputs=output)

def getNumpyList(images):
    res=[]
    for i in range(len(images)):
        temp=numpy.array(images[i])
        img=numpy.reshape(temp,(28,28))
        img=img.astype(dtype=numpy.float)/255.0
        res.append(img)
    return res


#load mnist data
mndata=mnist.MNIST('dataset')
images_train, labels_train = mndata.load_training()
images_test, labels_test = mndata.load_testing()


#create models
model,modelAe=createModel()

#this is the way of creating decoder model and copying weights from the autoencoder
decoder=createDecoderModel()
decoder.get_layer('decode1').set_weights(modelAe.get_layer('decode1').get_weights())
decoder.get_layer('decode2').set_weights(modelAe.get_layer('decode2').get_weights())
decoder.get_layer('decode3').set_weights(modelAe.get_layer('decode3').get_weights())

hidden_weights=model.get_layer('hidden').get_weights()
output_weights=model.get_layer('output').get_weights()

oneTransform=createOneTransformModel()
#load weights from the folder trans_weights/
for i in range(N):
    oneTransform.load_weights('trans_weights/trans'+str(i)+'.h5')
    weights=oneTransform.get_layer('hidden').get_weights()
    hidden_weights[0][:,i*512:(i*512+512)]=weights[0][:,:]
    hidden_weights[1][i*512:(i*512+512)]=weights[1][:]
    weights=oneTransform.get_layer('output').get_weights()
    output_weights[0][i,:,:]=weights[0][:,:]
    output_weights[1][i,0,:]=weights[1][:]
model.get_layer('hidden').set_weights(hidden_weights)
model.get_layer('output').set_weights(output_weights)

train_data=getNumpyList(images_train)
test_data=getNumpyList(images_test)


numpy_test_data=numpy.zeros((len(test_data),28,28))
for i in range(len(test_data)):
    numpy_test_data[i,:,:]=test_data[i][:,:]

numpy_test_data[0,:,:]=cv2.imread('V.bmp')[:,:,0]/255.0

#predict and save reconstructed images

result=model.predict(numpy_test_data)

#test 10 random input images
for j in range(10):
    index=random.randint(0,numpy_test_data.shape[0]-1)
    if j==0:
        index=0
    codes=result[0][index]
    image=numpy.zeros((9*28,45*28))
    for i in range(N):
        ix=int(i%45)
        iy=int(i/45)
        code=numpy.zeros((1,32))
        code[0,:]=codes[i,:]
        res=decoder.predict(code)
        image[iy*28:iy*28+28,ix*28:ix*28+28]=res[0][:,:]
    cv2.imwrite('reconstructions/'+str(j)+'.bmp',image*255.0)


