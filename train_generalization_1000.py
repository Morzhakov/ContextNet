from keras.layers import Input, Dense, Conv2D, Activation,LocallyConnected2D,MaxPooling2D,AveragePooling2D, UpSampling2D,Lambda,Multiply,Reshape,Dropout,concatenate,Flatten,Conv2DTranspose,BatchNormalization,Concatenate
from keras.models import Model
from keras.optimizers import Adam
import cv2
import os
import numpy
import mnist
import random


N=3*3*5*3*3

#prepair mnist images to numpy array
def getNumpyList(images):
    res=[]
    for i in range(len(images)):
        temp=numpy.array(images[i])
        img=numpy.reshape(temp,(28,28))
        img=img.astype(dtype=numpy.float)/255.0
        res.append(img)
    return res


##################
#fuctions for creating Keras model
##################
def createAeModel(input):
    x=Reshape((28*28,))(input)
    x=Dense(1024,activation='relu',name='encode1')(x)
    x=Dropout(0.1)(x)
    x=Dense(512,activation='relu',name='encode2')(x)
    x=Dropout(0.1)(x)

    code=Dense(32,activation='linear',name='encode3')(x)

    x=Dense(512,activation='relu',name='decode1')(code)
    x=Dropout(0.1)(x)
    x=Dense(1024,activation='relu',name='decode2')(x)
    x=Dropout(0.1)(x)
    x=Dense(784,activation='sigmoid',name='decode3')(x)
    output=Reshape((28,28))(x)

    return Model(inputs=input,outputs=[output,code]),code

def createGeneralizationModel(minicolumns,input):
    '''conv1=Conv2D(256,(1,1),activation='relu')(minicolumns)
    conv2=Conv2D(10,(1,1),activation='relu')(conv1)
    output=MaxPooling2D((405,1))(conv2)
    output=Reshape((10,))(output)'''



    conv1=Conv2D(256,(1,1),activation='relu')(minicolumns)
    conv1=MaxPooling2D((N,1))(conv1)
    conv1=Dropout(0.1)(conv1)
    conv1=Reshape((256,))(conv1)
    output=Dense(10,activation='softmax')(conv1)

    '''conv1=Conv2D(256,(1,1),activation='relu')(minicolumns)
    conv1=MaxPooling2D((405,1))(conv1)
    conv1=Reshape((256,))(conv1)
    output=Dense(10,activation='softmax')(conv1)'''

    return Model(inputs=input,outputs=output)

def createDecoderModel():
    code=Input(shape=(32,))
    x=Dense(512,activation='relu',name='decode1')(code)
    x=Dropout(0.1)(x)
    x=Dense(1024,activation='relu',name='decode2')(x)
    x=Dropout(0.1)(x)
    x=Dense(784,activation='sigmoid',name='decode3')(x)
    output=Reshape((28,28))(x)

    return Model(inputs=code,outputs=output)

def createTransformModel(code):
    hidden=Dense(N*512,activation='relu',name='hidden')(code)
    hidden_reshape=Reshape((N,1,512))(hidden)
    minicolumns_n=LocallyConnected2D(32,(1,1),activation='linear',name='output')(hidden_reshape)
    minicolumns=Reshape((N,32))(minicolumns_n)
    return minicolumns,minicolumns_n

def createModel():
    input = Input(shape=(28,28))
    modelAE,code=createAeModel(input)
    modelAE.load_weights('mnist_ae.h5')
    modelAE.trainable=False

    minicolumns,minicolumns_n=createTransformModel(code)

    return Model(inputs=input,outputs=[minicolumns,code]),modelAE,minicolumns_n,input

def createOneTransformModel():
    input = Input(shape=(32,))

    hidden=Dense(512,activation='relu',name='hidden')(input)
    output=Dense(32,activation='linear',name='output')(hidden)
    return Model(inputs=input,outputs=output)


##########################

def SetNotTrainable(model):
    model.get_layer('encode1').trainable=False
    model.get_layer('encode2').trainable=False
    model.get_layer('encode3').trainable=False
    model.get_layer('hidden').trainable=False
    model.get_layer('output').trainable=False
    return




#################
#create models
#################

model,modelAe,minicolumns,input=createModel()

#this is the way of creating decoder model and copying weights from the autoencoder
decoder=createDecoderModel()
decoder.get_layer('decode1').set_weights(modelAe.get_layer('decode1').get_weights())
decoder.get_layer('decode2').set_weights(modelAe.get_layer('decode2').get_weights())
decoder.get_layer('decode3').set_weights(modelAe.get_layer('decode3').get_weights())

hidden_weights=model.get_layer('hidden').get_weights()
output_weights=model.get_layer('output').get_weights()

oneTransform=createOneTransformModel()
#load weights from the folder trans_weights/
for i in range(N):
    oneTransform.load_weights('trans_weights/trans'+str(i)+'.h5')
    weights=oneTransform.get_layer('hidden').get_weights()
    hidden_weights[0][:,i*512:(i*512+512)]=weights[0][:,:]
    hidden_weights[1][i*512:(i*512+512)]=weights[1][:]
    weights=oneTransform.get_layer('output').get_weights()
    output_weights[0][i,:,:]=weights[0][:,:]
    output_weights[1][i,0,:]=weights[1][:]
model.get_layer('hidden').set_weights(hidden_weights)
model.get_layer('output').set_weights(output_weights)



generalization_model=createGeneralizationModel(minicolumns,input)
SetNotTrainable(generalization_model)

LR =0.001
generalization_model.compile(optimizer=Adam(lr=LR), loss="categorical_crossentropy")
#generalization_model.load_weights('generalization_1000_acc960.h5')

#############################

#load mnist data
mndata=mnist.MNIST('dataset')
images_train, labels_train = mndata.load_training()
images_test, labels_test = mndata.load_testing()


TRAIN_N=1000

numpy_train_data=numpy.zeros((TRAIN_N,28,28))
numpy_train_labels=numpy.zeros((TRAIN_N,10))
numpy_test_data=numpy.zeros((len(images_test),28,28))
numpy_test_labels=numpy.zeros((len(labels_test),10))

for i in range(TRAIN_N):
    numpy_train_data[i,:,:]=numpy.reshape(images_train[i],(28,28))[:,:]/255.0
    numpy_train_labels[i,labels_train[i]]=1.0
for i in range(len(images_test)):
    numpy_test_data[i,:,:]=numpy.reshape(images_test[i],(28,28))[:,:]/255.0
    numpy_test_labels[i,labels_test[i]]=1.0


for ep in range(2000):

    if ep==300:
        SetNotTrainable(generalization_model)
        LR=0.0001
        generalization_model.compile(optimizer=Adam(lr=LR), loss="categorical_crossentropy")
        SetNotTrainable(generalization_model)

    result=generalization_model.predict(numpy_test_data)
    succ=0
    N=0
    for i in range(numpy_test_data.shape[0]):
        if result[i].argmax()==labels_test[i]:
            succ+=1
        N+=1
    acc=succ/N
    print('acc '+str(acc))
    with open("acc_1000.txt", "a") as myfile:
        myfile.write(str(ep)+'\t'+str(acc)+'\n')
    generalization_model.fit(numpy_train_data,numpy_train_labels,batch_size=8,epochs=1)#,validation_data=[numpy_test_data,numpy_test_labels])

    generalization_model.save_weights('generalization_1000.h5')
    SetNotTrainable(generalization_model)




